# Immutable-detect

Test utility library for dynamically checking that an instance of
a data-structure is immutable. Meant to supplement Errorprone's `@Immutable`
annotation, for cases where statically immutable data structures would be
restricting, for example by throwing away the built-in list and map support in
Java.
