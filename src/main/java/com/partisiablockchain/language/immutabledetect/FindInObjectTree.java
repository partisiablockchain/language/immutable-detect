package com.partisiablockchain.language.immutabledetect;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Test utility for checking the existence of a value within a tree-structure.
 *
 * <p>This system is not generalized for all classes, nor for circular-graphs, it may produce
 * incorrect results or run forever in such cases. It is specifically designed for functional-like
 * data structures.
 *
 * <h2>Usage example</h2>
 *
 * <p>One powerful usage of {@link FindInObjectTree} is to check whether a given value or class is
 * covered by a set of test examples. This can be done with:
 *
 * <p>{@code Assertions.assertThat(examples).anyMatch(new
 * FindInObjectTree(MyFeature.class::isInstance)::anyMatch);}
 *
 * <p>{@code Assertions.assertThat(examples).anyMatch(new
 * FindInObjectTree(MyEnum.Feature::equals)::anyMatch);}
 */
public final class FindInObjectTree {

  private final Predicate<Object> objectPredicate;

  /**
   * Constructor for {@link FindInObjectTree}.
   *
   * @param objectPredicate Predicate for finding the object.
   */
  public FindInObjectTree(Predicate<Object> objectPredicate) {
    this.objectPredicate = objectPredicate;
  }

  /**
   * Performs recursive traversal of object references.
   *
   * @param obj Object.
   * @return Matching object.
   */
  public boolean anyMatch(final Object obj) {

    if (objectPredicate.test(obj)) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    final var cls = obj.getClass();

    // Records
    if (cls.isRecord()) {
      return Arrays.stream(cls.getRecordComponents())
          .map(
              field -> {
                final var accessor = field.getAccessor();
                accessor.setAccessible(true);
                return ExceptionConverter.call(
                    () -> accessor.invoke(obj), "Could not access object field");
              })
          .anyMatch(this::anyMatch);
    }

    // Collections
    if (obj instanceof Collection<?> objCollection) {
      return objCollection.stream().anyMatch(this::anyMatch);
    }

    // Maps
    if (obj instanceof Map<?, ?> map) {
      return Stream.concat(map.keySet().stream(), map.values().stream()).anyMatch(this::anyMatch);
    }

    return false;
  }
}
