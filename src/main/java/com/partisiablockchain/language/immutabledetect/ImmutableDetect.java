package com.partisiablockchain.language.immutabledetect;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Test utility for validating whether instantiations of data-structures are immutable.
 *
 * <p>This utility cannot check whether the data structure type in general is immutable, but it can
 * check whether specific objects are.
 *
 * <p>Meant to supplement the statically checked {@link Immutable} annotations.
 */
public final class ImmutableDetect {

  private ImmutableDetect() {}

  private static final FindInObjectTree FIND_MUTABLE =
      new FindInObjectTree(ImmutableDetect::isMutableVisit);

  /**
   * Tests that the given object is structurally immutable, such that it and its contents cannot be
   * modified. Bias towards outputting mutable. Will attempt to modify some objects; inputs might be
   * in an unpredictable state if method outputs mutable.
   *
   * @param rootObject Object to test.
   * @return True if the object is immutable. Always true if null.
   */
  public static boolean isImmutable(final Object rootObject) {
    return !FIND_MUTABLE.anyMatch(rootObject);
  }

  /**
   * Function to check whether the given object is mutable. It does not recurse; even though fields
   * or elements within the datastructure could contain mutability.
   *
   * @param obj Object to checked for mutability.
   * @return true if the immediate object allows for mutability. false otherwise.
   */
  private static boolean isMutableVisit(final Object obj) {

    // Null objects
    if (obj == null) {
      return false;
    }

    final var cls = obj.getClass();

    // Enums
    if (cls.isEnum()) {
      return false;

      // Annotated immutable
    } else if (cls.getAnnotation(Immutable.class) != null) {
      return false;
    }

    // Records
    if (cls.isRecord()) {
      return false;
    }

    // Collections
    if (obj instanceof Collection<?> objCollection) {
      try {
        objCollection.clear();
        return true;
      } catch (final UnsupportedOperationException e) {
        // Success: Could not change collection
        return false;
      }
    }

    // Path (All path implementations must be immutable.)
    if (obj instanceof Path) {
      return false;
    }

    // Maps
    if (obj instanceof Map<?, ?> map) {
      try {
        map.clear();
        return true;
      } catch (final UnsupportedOperationException e) {
        // Success: Could not change map
        return false;
      }
    }

    // Well-known class
    if (GUARANTEED_IMMUTABLE.contains(cls)) {
      return false;
    }

    // Any other class
    return true;
  }

  /** Set of object classes that guarentee immutability. */
  private static Set<Class<?>> GUARANTEED_IMMUTABLE =
      Set.of(
          Boolean.class,
          Byte.class,
          Character.class,
          Short.class,
          Integer.class,
          Long.class,
          Float.class,
          Double.class,
          BigInteger.class,
          BigDecimal.class,
          String.class);
}
