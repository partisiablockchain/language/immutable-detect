package com.partisiablockchain.language.immutabledetect;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.immutabledetect.examples.ExamplesOfImmutable;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/** Detect whether {@link ImmutableDetect} can correctly infer mutability. */
public final class ImmutableDetectTest {

  static Stream<Object> provideImmutableObjects() {
    return ExamplesOfImmutable.EXAMPLES_IMMUTABLE.stream();
  }

  static Stream<Object> provideMutableObjects() {
    return ExamplesOfImmutable.EXAMPLES_MUTABLE.stream();
  }

  /** Null is immutable. */
  @Test
  public void immutableNull() {
    final Object obj = null;
    assertThat(obj).matches(ImmutableDetect::isImmutable, "is immutable");
  }

  /** Immutable objects are detected as immutable. */
  @ParameterizedTest
  @MethodSource("provideImmutableObjects")
  public void immutableObjects(Object obj) {
    assertThat(obj).matches(ImmutableDetect::isImmutable, "is immutable");
  }

  /** Mutable objects are not detected as immutable. */
  @ParameterizedTest
  @MethodSource("provideMutableObjects")
  public void mutableObjects(Object obj) {
    assertThat(obj).matches(Predicate.not(ImmutableDetect::isImmutable), "is mutable");
  }
}
