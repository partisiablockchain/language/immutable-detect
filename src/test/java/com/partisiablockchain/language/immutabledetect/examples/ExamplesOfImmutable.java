package com.partisiablockchain.language.immutabledetect.examples;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/** Various examples of both mutable and immutable objects. */
public final class ExamplesOfImmutable {

  private static final MyRecord RECORD_1 = new MyRecord(42, "hello world");
  private static final MyRecord RECORD_2 = new MyRecord(231, "hello inner world");

  /** Immutable example objects. */
  public static final List<Object> EXAMPLES_IMMUTABLE =
      List.of(
          // Primitives and builtins
          1,
          2L,
          true,
          'c',
          (short) 3,
          9.3f,
          4.6d,
          new BigInteger("312213"),
          "Hello world!",

          // Enums
          MyEnum.A,
          MyEnum.B,
          MyEnum.C,

          // Records
          RECORD_1,
          new MyOuterRecord(RECORD_2),
          new Wrapper<>(231),
          new Wrapper<MyImmutableClass>(null),
          new Wrapper<>("helloest world"),
          new Wrapper<>(new MyOuterRecord(RECORD_2)),

          // Classic classes
          new MyImmutableClass(123, "xtz"),

          // Java SDK Collections
          List.of(21, 3215, 234, 56),
          Set.of(231, 321, 432),
          List.of(new MyImmutableClass(3221, "abc")),
          Map.of(321, 13, 412, 5342),

          // Java SDK stream derived
          Stream.of(1, 9, 39, 49, 54121).toList(),
          Stream.of(2, 9, 39, 49, 54121).collect(Collectors.toUnmodifiableList()),
          Stream.of(3, 9, 39, 49, 54121)
              .collect(Collectors.toUnmodifiableMap(Function.identity(), Function.identity())),
          Stream.of(4, 9, 39, 49, 54121).collect(Collectors.toUnmodifiableSet()),

          // Java Path
          Path.of("some", "path", "here"),

          // Internal record
          new MyInternalRecord(321),

          // Empty Immutable structures containing mutables
          List.<MyEmptyClass>of(),
          Map.<Integer, MyEmptyClass>of(),

          // Records with inner collections
          new RecordWithList(List.of(RECORD_1, RECORD_2)));

  /** Mutable example objects. */
  @SuppressWarnings("UnnecessaryStringBuilder")
  public static final List<Object> EXAMPLES_MUTABLE =
      List.of(
          // Classic classes
          new MyEmptyClass(),
          new MyMutableClass(),
          new Wrapper<>(new MyEmptyClass()),

          // Java SDK Collections
          new ArrayList<MyRecord>(),
          new ArrayList<>(List.of(RECORD_1, RECORD_2)),
          new HashMap<Integer, MyRecord>(),
          new HashMap<>(Map.of(12, RECORD_1, 32, RECORD_2)),

          // Immutable structures containing mutables
          List.of(new MyEmptyClass(), new MyEmptyClass(), new MyEmptyClass(), new MyEmptyClass()),
          Map.of(
              0,
              new MyEmptyClass(),
              1,
              new MyEmptyClass(),
              2,
              new MyEmptyClass(),
              3,
              new MyEmptyClass()),

          // Java SDK Stream and derived
          IntStream.range(1, 10),
          Stream.of(1, 2, 3, 4, 6),
          List.of(1, 231, 23, 32).stream(),
          Stream.of(2, 9, 39, 49, 54121).collect(Collectors.toList()),
          Stream.of(3, 9, 39, 49, 54121)
              .collect(Collectors.toMap(Function.identity(), Function.identity())),
          Stream.of(4, 9, 39, 49, 54121).collect(Collectors.toSet()),

          // Java SDK misc
          Function.<MyRecord>identity(),
          new StringBuilder("hello worlder"),
          new ByteArrayOutputStream(),

          // Arrays
          new Wrapper<>(new boolean[] {true, false}),
          new Wrapper<>(new byte[2]),
          new Wrapper<>(new char[2]),
          new Wrapper<>(new double[2]),
          new Wrapper<>(new float[2]),
          new Wrapper<>(new int[] {1, 4, 6, 1, 55}),
          new Wrapper<>(new long[2]),
          new Wrapper<>(new short[2]),
          new Wrapper<>(new Object[2]),
          new Wrapper<>(new MyEnum[] {MyEnum.A, MyEnum.B, MyEnum.C}),
          new Wrapper<>(new int[321]),

          // Records with inner collections
          new RecordWithList(new ArrayList<>(List.of(RECORD_1, RECORD_2))));

  ////

  /** Example class. */
  public enum MyEnum {
    A,
    B,
    C
  }

  /** Example class. */
  @SuppressWarnings("unused")
  public record MyRecord(int a, String b) {
    private static final int SOME_CONSTANT = 0;
  }

  /** Example class. */
  @SuppressWarnings("unused")
  public record MyOuterRecord(MyRecord inner) {
    private static int SOME_GLOBAL = 0;
  }

  /** Example class. */
  public record RecordWithList(List<MyRecord> ls) {}

  /** Example class. */
  public record Wrapper<T>(T inner) {}

  /** Example class. */
  record MyInternalRecord(int value) {}

  /** Example class. */
  public static final class MyMutableClass {
    public int field = 0;
  }

  /** Example class. */
  public static final class MyEmptyClass {}

  /** Example class. */
  @Immutable
  @SuppressWarnings("unused")
  public static final class MyImmutableClass {
    final int myField;
    final String myField2;

    private static final MyMutableClass SOME_OTHER_CONSTANT = new MyMutableClass();

    private static int SOME_GLOBAL = 0;

    /**
     * Constructor of {@link MyImmutableClass}.
     *
     * @param myField First field. Nullable.
     * @param myField2 Second field. Nullable.
     */
    public MyImmutableClass(int myField, String myField2) {
      this.myField = myField;
      this.myField2 = myField2;
    }
  }
}
